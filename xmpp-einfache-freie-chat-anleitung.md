# SofortNachrichten (Instant Messenger) mit XMPP

Die datenschutzfreundliche förderale Ergänzung (und Alternative) zu WhatsApp & Co.

https://www.Freie-Messenger.de und https://www.kuketz-blog.de/die-verrueckte-welt-der-messenger-messenger-teil1/ geben gute Infos zum Freien Chat. Wenn meine Erklärungen nicht kurz oder ausführlich oder klar genug sind, lies gern dort weiter.


## 1. SCHRITT: XMPP Anbieter aussuchen

Du kannst beliebig viele (kostenfreie) xmpp-Adressen haben (auch JID - Jabber-IDentifikation genannt.)

Bei "Apple iOS iPhone" nehme vorzugsweise https://tigase.im (z.B. mit der App Siskin oder ChatSecure). Siehe https://www.Freie-Messenger.de/sys_xmpp/apple/

Wenn du anstatt eine (anonyme) von dir selbst ausgedachte Adresse (xmpp:zeichen@anbieter.de) deine Telefonnummer als Identifikations-Merkmal nutzen möchtest, dann schau dir den Anbieter und App https://quicksy.im an. Einfach wie bei WhatsApp. Aber willst du jedem Chat-Kontakt deine Telefonnummer geben?

Datensparsamer ist folgendes:
- **" https://mailbox.org "** (samt Mail 1€/Monat) oder
- **" https://wiuwiu.de " (0€)** oder 
- https://blabber.im (0€) oder 
- Liste auf www.Freie-Messenger.de

(Bei manchen Apps kannst du auch gleich dort ein Account erstellen, manchmal kostenfrei, manchmal mit Auswahl.)
(Anbieter und App-Programmierer freuen sich über energieausgleich durch Geld.)

### mailbox.org
dort ist die eigene E-Mail-Adresse und die XMPP-Adresse die selbe. Eigene Domain möglich. XMPP-Infos: https://kb.mailbox.org/m/view-rendered-page.action?abstractPageId=1181160


## 2. SCHRITT: Installiere auf deinen Geräten jeweils eine XMPP App

(Jede dieser xmpp-Clienten (Apps) ist auf wenige Funktionen begrenzt. Ähnlich wie der E-Mail-Client (wie Thunderbird). Anders sind die Web-Browser (wie Firefox). Sie sind gut gewartet und Freie Software. Daher ist die Nutzung von xmpp tendenziell sicherer als die Nutzung vom Web-Browser, geschweige denn )
 
### Android
Bei *Android* installiere einfach A) anonym www.Conversations.im ODER B) www.Quicksy.im mit Telefonnummer als Id.
(Im Open Source www.F-Droid.org - Store sind alle Apps legal kostenfrei. Auch Conversations. Spende sinnvoll. Im Google Playstore ggf. ohne "www." und ".im" suchen.)

### Apple iOS iPhone
Bei *Apple iOS iPhone* kannst du ChatSecure oder Siskin (oder Monal) nehmen. Siehe https://www.Freie-Messenger.de/sys_xmpp/apple

### Linux
Bei *Linux* kannst du  www.dino.im (Paket dino-im) nutzen (einfach, modern). Oder gajim.org (Pidgin nicht erste wahl.)

### Microsoft Windows
Bei *MS Windows* fällt mir erstmal gajim.org ein. (Pidgin ist für viele nicht erste Wahl, wenn es um XMPP mit OMEMO Verschlüsselung geht.)

Die Suchbegriffe **"XMPP OMEMO"** und möglichst **"Open Source"** (Freie Software) sollten ausreichen. Oder auch schon **www.Freie-Messenger.de**

## weiterführende Infos

Als mündiger Nutzer von Computern (inkl. Smartphone) ist es meines Erachtens sinnig die Begriffe "Freeware" und "Freie Software" (Open Source) und "Unfreie Software" (Proprietär) sowie "offene" und "geschlossene Plattform" (System, Walled Garden) zu kennen.

TRANSPARENZ UND MITBESTIMMUNG sind zentrale Werte bei der GWÖ und der Entwicklung von "Free/Libre Open Source Software" (FLOSS). 

Die "Protokoll-Sprache" "XMPP" ist "offen Dokumentiert" und unter "Freier Lizenz". Deshalb sind XMPP-Server als auch -Clienten (Apps, AnwenderProgramme) Freie Software. XMPP ist "förderal": Jeder kann mit jedem kommunizieren, egal welcher Anbieter. Wie E-Mail und Telefon. Eigener Server aufsetzen oder aufsetzen lassen ist möglich. Dieser kann offen ins förderale xmpp-Netz sein oder geschlossen wie z.B. die Walled Garden WhatsApp.

**Open Source** (offene Quelle) bezeichet Software (OSS), deren Quelltext öffentlich und von Dritten eingesehen, geändert und genutzt werden kann. Open-Source-Software kann meistens kostenlos genutzt werden.

**Freie Software** darf für jeden Zweck ausgeführt, untersucht, modifiziert und weiterverbreitet werden. Dieses Lizenzkonzept ist **freiheitsgewährend** oder gar **freizügig** und sollte nicht mit kostenfreier Freeware  verwechselt werden.

**FLOSS** - Free/Libre Open Source Software ist ein Sammelbegriff für Freie Software und Open-Source-Software.

Eine **geschlossene Plattform / System** (Walled Garden) ist vom Hersteller mit Restriktionen, Einschränkungen versehen. Im Gegensatz dazu stehen **offene Plattformen** , die dem Benutzer viele Freiheiten gewähren. https://de.m.wikipedia.org/wiki/Geschlossene_Plattform

https://www.kuketz-blog.de/die-verrueckte-welt-der-messenger-messenger-teil1 und 
https://Freie-Messenger.de fassen Infos treffend zusammen.
