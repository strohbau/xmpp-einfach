# xmpp-einfach

Einfache Erklärung von SofortNachrichten (Instant Messenger) über XMPP. Siehe https://www.Freie-Messenger.de

https://gitlab.com/strohbau/xmpp-einfach/tree/master/xmpp-einfache-freie-chat-anleitung.md

## Vergleiche von Messengern
findest du bei ...

https://www.freie-messenger.de/systemvergleich/
https://www.freie-messenger.de/systemvergleich/externe_vergleiche/

Am Rande dürfen im laufe der Zeit Vergleiche von Messengern und tiefere Infos über FLOSS, förderale Systeme und offene Platformen wie XMPP und Matrix samt www.Riot.im gesammelt werden.

Hauptsächlich soll es hier aber um XMPP gehen.

https://gitlab.com/strohbau/xmpp-einfach/tree/master/xmpp-einfache-freie-chat-anleitung.md
